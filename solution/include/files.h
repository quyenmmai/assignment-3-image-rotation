#ifndef FILES_H
#define FILES_H

#include <stdio.h>

enum close_status {
	CLOSE_SUCCESS,
	CLOSE_UNKNOWN_ERROR,
    CLOSE_FILE_NOT_EXIST
	
	
};

enum open_status {
	OPEN_SUCCESS,
	OPEN_UNKNOWN_ERROR,
    OPEN_FILE_NOT_EXIST,
    OPEN_FILE_PERMISSION_DENIED
	
};

enum close_status file_close(FILE** file);


enum open_status file_open(FILE** file, const char* name, const char* mode);



#endif
