#include "../include/bmp.h"
#include <stdint.h>
#define NUM 0xFF
#define NUM4 40

#include <stdbool.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfSignature;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)
static bool check_signature(const struct bmp_header* header) {
    if ((header->bfSignature & NUM) == 'B' && ((header->bfSignature >> 8) & NUM) == 'M') 
        return true;
    return false;
}
static uint8_t create_padding(uint32_t width) {
    if(width % 4 == 0) return 0;
    return (4 - (width * sizeof(struct pixel)) % 4);
}


enum bmp_status from_bmp(FILE* file, struct image* const img) {
    struct bmp_header header;
    size_t result = fread(&header, sizeof(struct bmp_header), 1, file);
    
    if (!check_signature(&header)) return BMP_INVALID_HEADER;

    if (result != 1) return BMP_ERROR;

    *img = create_image(header.biWidth, header.biHeight);
    if(img == NULL) return BMP_ERROR;
    for (uint32_t y = 0; y < img->height; y++) {
        size_t result = fread(find_pixel_in_image(0, y, img), sizeof(struct pixel), img->width, file);
        
        if (result != img->width) return BMP_ERROR;

        result = fseek(file, create_padding(img->width), SEEK_CUR);
        if (result != 0) return BMP_ERROR;
    }

    return BMP_SUCCESS;
}

struct bmp_header create_header(uint32_t width, uint32_t height) {
    const uint32_t IMAGE_SIZE = (sizeof(struct pixel) * width + create_padding(width)) * height;
    return (struct bmp_header) {
            .bfSignature = ('M' << 8) + 'B',
            .bfileSize = 14 + NUM4 + IMAGE_SIZE,
            .bfReserved = 0,
            .bOffBits = 14 + NUM4,
            .biSize = NUM4,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = IMAGE_SIZE,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

enum bmp_status to_bmp(FILE* file, struct image const img) {
    struct bmp_header header = create_header(img.width, img.height);
    size_t result = fwrite(&header, sizeof(struct bmp_header), 1, file);
    
    if (result != 1) return BMP_ERROR;
    
    for (uint32_t y = 0; y < img.height; y++) {
        size_t result = fwrite(find_pixel_in_image(0, y, &img), sizeof(struct pixel), img.width, file);
        if (result != img.width) return BMP_ERROR;

        for (int i = 0; i < create_padding(img.width); i++) {
            result = putc(0, file);
            if (result == EOF) return BMP_ERROR;
        }
    }

    return BMP_SUCCESS;
}
