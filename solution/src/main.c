#include "../include/bmp.h"
#include "../include/files.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
FILE* file;
enum bmp_status bmp_status;
enum close_status close_status;
enum open_status open_status;
struct image result_image = {0};
struct image source_image = {0};


static const char* const open_error_messages[] = {
        "PERMISSION DENIED\n",
        "FILE DOES NOT EXIST\n",
        "UNKNOWN ERROR"
};

static const char* const close_error_messages[] = {
        " NO SUCH FILE\n",
        "UNKNOWN ERROR"
};
static const char* const bmp_error_messages[] = {
        "FILE FORMAT MUST BE IS BMP\n",
        "UNKNOWN ERROR"
};




bool close_status_check(enum close_status close_status) {
    if (close_status != CLOSE_SUCCESS) {
				printf( "%s\n" , close_error_messages[close_status]);
        exit(1);
    }
    return 0;
}

bool bmp_status_check(enum bmp_status bmp_status) {
    if (bmp_status != BMP_SUCCESS) {
				printf( "%s\n" , bmp_error_messages[bmp_status]);
        exit(1);
    }
    return 0;
}
bool open_status_check(enum open_status open_status) {
    if (open_status != OPEN_SUCCESS) {
				printf( "%s\n", open_error_messages[open_status]);
        exit(1);
    }
    return 0;
}

void get_result(char* argv[]) {
		
		open_status = file_open(&file, argv[2], "wb");
    
    if (open_status_check(open_status)) {
        destroy_image(&result_image);
       exit(1);
    }

    bmp_status = to_bmp(file, result_image);

    close_status = file_close(&file);

    if (close_status_check(close_status)){
        exit(1);
    }

    destroy_image(&result_image);

    if (bmp_status_check(bmp_status)) {
        exit(1);
    }
}

void check(int argc, char* argv[]) {
	if (argc != 3) {
        printf( "Manual for using this program:\n ./image-transformer source_image.bmp transformed_image.bmp\n" );
        exit(1);
    }

    open_status = file_open(&file, argv[1], "rb");

    if (open_status_check(open_status)) {
        exit(1);
    }

    bmp_status = from_bmp(file, &source_image);
    close_status = file_close(&file);

    if (close_status_check(close_status)){
       exit(1);
    }

    if (bmp_status_check(bmp_status)) {
        destroy_image(&source_image);
        exit(1);
    }
}



int main(int argc, char* argv[]) {
		printf("Image rotation\n");

		check(argc, argv);
 
    result_image = rotate(source_image);
		get_result(argv);
		printf( "ROTATED.");
    return 0;
}
